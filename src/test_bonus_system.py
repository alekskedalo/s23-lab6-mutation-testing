
from bonus_system import calculateBonuses
import pytest

@pytest.mark.parametrize("program,amount,expected", [
    (None, 0, 0),
    (None, 10000, 0.0),
    (None, 25000, 0.0),
    (None, 50000, 0),
    (None, 75000, 0),
    (None, 100000, 0.0),
    (None, 200000, 0.0),
    ('nothing', 0, 0),
    ('nothing', 10000, 0.0),
    ('nothing', 25000, 0.0),
    ('nothing', 50000, 0),
    ('nothing', 75000, 0),
    ('nothing', 100000, 0.0),
    ('nothing', 200000, 0.0),
    ('Standard', 0, 0.5),
    ('Standard', 10000, 0.75),
    ('Standard', 25000, 0.75),
    ('Standard', 50000, 1.0),
    ('Standard', 75000, 1.0),
    ('Standard', 100000, 1.25),
    ('Standard', 200000, 1.25),
    ('Premium', 0, 0.1),
    ('Premium', 10000, 0.15000000000000002),
    ('Premium', 25000, 0.15000000000000002),
    ('Premium', 50000, 0.2),
    ('Premium', 75000, 0.2),
    ('Premium', 100000, 0.25),
    ('Premium', 200000, 0.25),
    ('Diamond', 0, 0.2),
    ('Diamond', 10000, 0.30000000000000004),
    ('Diamond', 25000, 0.30000000000000004),
    ('Diamond', 50000, 0.4),
    ('Diamond', 75000, 0.4),
    ('Diamond', 100000, 0.5),
    ('Diamond', 200000, 0.5),
])

def test_bonus_system(program, amount, expected):
    result = calculateBonuses(program, amount)
    assert result == expected

